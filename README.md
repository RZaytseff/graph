# README #

====================
### Постановка задачи: ###
--------------------
Simple Graph lib:

Should support 2 types of graphs - directed and undirected with 3 operations:

  addVertex - adds vertex to the graph

  addEdge - adds edge to the graph

  getPath - returns a list of edges between 2 vertices (path doesn’t have to be optimal)

  Vertices should be of a user defined type.

 Add weighted edges support in your lib.
 Add traverse function that will take a user defined function and apply it on every vertex of the graph.
 Make you graphs thread safe.
=====================================================
### Решение задачи. ###
-------
Решение задачи представлено в трёх пакетах:
grapf.lib - исходная библиотека
grapf.ext - расширение функционала библиотеки, а именно: добавление взвешенных графов и функция обхода всех узлов графа.
grapf.zrv - пакет реализует заданный функционал полнстью с НУЛЯ.

Для каждого пакета разработаны тесты, в том числе проверяющие работу функции getPath для 4-х видов реализованных графов при высокой  нагрузе, создаваемой параллельно выполняемыми в отдельных потококах операциями модификации графа, т.е. удаление и добваление вершин и рёбер графа.  

запуск тестов и последующей сборки выполняется из папки размещения проекта командой:
> mvn install

Подробное описание всех пакетов программ и тестов к ним приведено в документе README.doc
=======================================================

### Результаты тестирования приведены ниже: ###
---------------------------------------------

-----------------> Begin of UnDirect Graph Test
Vertex#4-Float  (4.0) -> Vertex#0-Integer      , Path steps = 2 , Path Distance = Infinity, Path: Vertex#4-Float  (4.0) -> Vertex#8-Float  (8.0) -> Vertex#0-Integer
Vertex#0-Integer      -> Vertex#4-Float  (4.0) , Path steps = 2 , Path Distance = Infinity, Path: Vertex#0-Integer      -> Vertex#8-Float  (8.0) -> Vertex#4-Float  (4.0)
-----------------> End of UnDirect Graph Test: Duration = 31

-----------------> Begin of Directed and Weighted Graph Test
Vertex#4-Float  (4.0) -> Vertex#0-Integer      , Path steps = 4 , Path Distance = 33.0, Path: Vertex#4-Float  (4.0) -> Vertex#5-Float  (5.0) -> Vertex#6-Float  (6.0) -> Vertex#8-Float  (8.0) -> Vertex#0-Integer
Vertex#0-Integer      -> Vertex#4-Float  (4.0) , Path steps = 4 , Path Distance = 28.0, Path: Vertex#0-Integer      -> Vertex#1-Double (1.0) -> Vertex#2-Float  (2.0) -> Vertex#3-Float  (3.0) -> Vertex#4-Float  (4.0)
-----------------> End of Directed and Weighted Graph Test: Duration = 3

-----------------> Begin of traverse Graph Test
For Vertex#0-Integer      result is Vertex#0-Integer     .result
For Vertex#1-Double (1.0) result is Vertex#1-Double .result(1.0)
For Vertex#2-Float  (2.0) result is Vertex#2-Float  .result(2.0)
For Vertex#3-Float  (3.0) result is Vertex#3-Float  .result(3.0)
For Vertex#4-Float  (4.0) result is Vertex#4-Float  .result(4.0)
For Vertex#5-Float  (5.0) result is Vertex#5-Float  .result(5.0)
For Vertex#6-Float  (6.0) result is Vertex#6-Float  .result(6.0)
For Vertex#7-Float  (7.0) result is Vertex#7-Float  .result(7.0)
For Vertex#8-Float  (8.0) result is Vertex#8-Float  .result(8.0)
-----------------> End of dijkstraDirectWeighted Graph Test: Duration = 81

-----------------> Begin of UnDirected and Weighted Graph Test
Vertex#4-Float  (4.0) -> Vertex#0-Integer      , Path steps = 4 , Path Distance = 21.0, Path: Vertex#4-Float  (4.0) -> Vertex#5-Float  (5.0) -> Vertex#6-Float  (6.0) -> Vertex#7-Float  (7.0) -> Vertex#0-Integer
Vertex#0-Integer      -> Vertex#4-Float  (4.0) , Path steps = 4 , Path Distance = 21.0, Path: Vertex#0-Integer      -> Vertex#7-Float  (7.0) -> Vertex#6-Float  (6.0) -> Vertex#5-Float  (5.0) -> Vertex#4-Float  (4.0)
-----------------> End of UnDirected and Weighted Graph Test: Duration = 1

-----------------> Begin of Directed Graph Test
Vertex#4-Float  (4.0) -> Vertex#0-Integer      , Path steps = 4 , Path Distance = Infinity, Path: Vertex#4-Float  (4.0) -> Vertex#5-Float  (5.0) -> Vertex#6-Float  (6.0) -> Vertex#8-Float  (8.0) -> Vertex#0-Integer
Vertex#0-Integer      -> Vertex#4-Float  (4.0) , Path steps = 3 , Path Distance = Infinity, Path: Vertex#0-Integer      -> Vertex#7-Float  (7.0) -> Vertex#8-Float  (8.0) -> Vertex#4-Float  (4.0)
-----------------> End of Directed Graph Test: Duration = 2
