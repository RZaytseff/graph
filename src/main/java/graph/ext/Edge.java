package graph.ext;

/** Class represent the edges in the graph */
public class Edge extends graph.lib.Edge {

	private final float weight;

	public Edge(Vertex target, float weight){
		super(target);
		this.weight = weight;
	}

	public Vertex getTarget() {
		return (Vertex)super.getTarget();
	}

	public float getWeight() {
		return weight;
	}
}
