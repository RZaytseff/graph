package graph.ext;

import java.util.List;
import java.util.function.Consumer;

public interface Graph {
    void addVertex(Vertex vertex);

    Vertex getVertex(int vertexIndex);

    List<Vertex> getVertexes();

    void deleteVertex(Vertex vertex);

    boolean addEdge(int src, int dest, float weight);

    void traverse(Consumer<Vertex> userDefineConsumer);

    Path getPath(Vertex source, Vertex destination);
}
