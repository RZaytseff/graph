package graph.ext;

public interface Algorithm {
    Path getPath(Vertex source, Vertex destination);
}
