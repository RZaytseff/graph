package graph.ext;
/**
 * Implementation of Dijkstra's Algorithm - Adjacency List and Priority Queue.
 */

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.PriorityBlockingQueue;

/** find shortest path according summarise weight */
public class DijkstraWeightedAlgorithm implements Algorithm {

    public Path getPath(Vertex source, Vertex destination) {
        // Algorithm:
        // 1. Take the unvisited node with minimum weight.
        // 2. Visit all its neighbours.
        // 3. Update the distances for all the neighbours (In the Priority Queue).
        // Repeat the process while achieve destination vertex and all connected nodes are visited.

        Map<Vertex, Path> paths = new HashMap<>();
        paths.put(source, new Path(0, 0));
        Path path;

        PriorityBlockingQueue<Vertex> queue = new PriorityBlockingQueue<>();
        queue.add(source);

        Vertex v;
        while(!queue.isEmpty() && (v = queue.poll()) != null) {
//            synchronized (v.getNeighbours()) {
                Path currentPath = paths.get(v);
                for (Edge neighbour : (List<Edge>) v.getNeighbours()) {
                    int newLevel = currentPath.getLevel();
                    Float newDist = currentPath.getMinDistance() + neighbour.getWeight();
                    if (paths.get(neighbour.getTarget()) == null) {
                        paths.put(neighbour.getTarget(), new Path());
                    }
                    Vertex vertexDestination = neighbour.getTarget();
                    if (paths.get(vertexDestination).getMinDistance() > newDist) {
                        // Remove the node from the queue to update the distance value.
                        queue.remove(vertexDestination);
                        paths.get(vertexDestination).setLevel(++newLevel);
                        paths.get(vertexDestination).setMinDistance(newDist);

                        // Take the path visited till now and add the new vertex
                        paths.get(vertexDestination).setVertexes(new LinkedList<Vertex>(paths.get(v).getVertexes()));
                        paths.get(vertexDestination).getVertexes().add(v);

                        //Reenter the node with new distance.
                        queue.add(vertexDestination);
                        if (vertexDestination.equals(destination)) {
                            break;
                        }
                    }
                }
//            }
        }
        path = paths.get(destination);
        if (path == null) {
            path = new Path();
            path.getVertexes().add(source);
        }
        path.getVertexes().add(destination);
        return path;
    }
}

