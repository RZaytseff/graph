package graph.ext;

import java.util.List;
import java.util.function.Consumer;

public class GraphFabric extends graph.lib.GraphFabric implements Graph {
    private boolean weighted;
    Algorithm algorithm;
    graph.lib.GraphFabric algorithmLib;

    public GraphFabric(boolean directed, boolean weighted) {
        super(directed);
        this.weighted = weighted;
        if(weighted) {
            algorithm = new DijkstraWeightedAlgorithm();
        } else {
            algorithmLib = new graph.lib.GraphFabric(directed);
        }
    }

    public void addVertex(Vertex vertex) {
        getVertexes().add(vertex);
    }

    public boolean addEdge(int src, int dest, float weight) {
        if (src >= getVertexes().size() || dest  >= getVertexes().size()) {
            return false;
        }
        Vertex s = (Vertex)getVertexes().get(src);
        Vertex d = (Vertex)getVertexes().get(dest);
        if(s == null || d == null) {
            return false;
        }
        Edge edge = new Edge(d, weight);
        s.getNeighbours().add(edge);

        if (!isDirected()) {
            graph.lib.Edge return_edge = new Edge(s, weight);
            d.getNeighbours().add(return_edge);
        }
        return true;
    }

    public void traverse(Consumer<Vertex> f) {
        for (Object vertex : getVertexes()) {
            System.out.print("For " + vertex);
            f.accept((Vertex) vertex);
            System.out.println(" result is " + vertex);
        }
    }

    public Path getPath(Vertex source, Vertex destination) {
        if(weighted) {
            return algorithm.getPath(source, destination);
        } else {
            graph.lib.Path pathLib = algorithmLib.getPath(source, destination);
            Path path = new Path(pathLib.getLevel());
            path.setVertexes(path.getVertexes());
            return path;
        }
    }

     public Vertex getVertex(int vertex){
        return (Vertex)getVertexes().get(vertex);
    }

    public void deleteVertex(Vertex vertex) {
        getVertexes().remove(vertex);
    }

    public List getVertexes() {
        return super.getVertexes();
    }
}
