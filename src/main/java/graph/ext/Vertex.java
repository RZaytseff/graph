package graph.ext;

public class Vertex<Type> extends graph.lib.Vertex<Type> implements Comparable<graph.lib.Vertex> {

    public Vertex(String name) {
        super(name);
    }

    public Vertex(String name, Type value) {
        super(name, value);
    }

    @Override
    public synchronized int compareTo(graph.lib.Vertex other){
        if(!(this.getClassType().equals(other.getClassType()))) {
            return 1;
        } else if(getName() != null && !getName().equals(other.getName())) {
            return 1;
        } else if(getValue() != null && !getName().equals(other.getValue())) {
            return -1;
        } else {
            int countInclude = 0;
            for (graph.lib.Edge neighbour : getNeighbours()) {
                if (!other.getNeighbours().contains(neighbour)) {
                    countInclude++;
                }
            }
            int countExclude = 0;
            for (Object neighbour : other.getNeighbours()) {
                if (!getNeighbours().contains(neighbour)) {
                    countExclude--;
                }
            }
            if (countInclude != 0 || countExclude != 0) {
                return (countInclude == 0 ? 1 : countInclude) * (countExclude == 0 ? 1 : countExclude);
            } else {
                return countInclude * countExclude;
            }
        }
    }

}
