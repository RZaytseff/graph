package graph.lib;

/** Class represent the edges in the graph */
public class Edge {
	private final Vertex target;

	public Edge(Vertex target) {
		this.target = target;
	}

    public Vertex getTarget() {
        return target;
    }

}
