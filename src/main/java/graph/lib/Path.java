package graph.lib;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Path {
    private List<Vertex> vertexes = new CopyOnWriteArrayList<>();
    private int level = Integer.MAX_VALUE - 1;
    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
