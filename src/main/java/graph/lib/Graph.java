package graph.lib;

public interface Graph {
    void addVertex(Vertex vertex);

    Vertex getVertex(int vertexIndex);

    void deleteVertex(Vertex vertex);

    boolean addEdge(int src, int dest);

    Path getPath(Vertex source, Vertex destination);
}
