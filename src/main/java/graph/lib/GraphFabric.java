package graph.lib;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.PriorityBlockingQueue;

public class GraphFabric implements Graph {
    private boolean directed;

    /** Each vertex maps to a list of all his neighbors */
	private List<Vertex> vertices;

    public GraphFabric(){
        this.vertices = new CopyOnWriteArrayList<>();
    }

    public GraphFabric(boolean directed){
        this();
        this.directed = directed;
	}

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
    }

    public void deleteVertex(Vertex vertex) {
        vertices.remove(vertex);
    }

    public boolean addEdge(int src, int dest) {
		Vertex s = vertices.get(src);
        Vertex d = vertices.get(dest);
        if(s == null || d == null) {
            return false;
        }
            Edge edge = new Edge(d);
            s.getNeighbours().add(edge);

            if (!directed) {
                Edge return_edge = new Edge(s);
                d.getNeighbours().add(return_edge);
            }
		return true;
	}

    /*
     Algorithm:
     1. Take the unvisited node with shortest path.
     2. Visit all its neighbours.
     3. Update the distances for all the neighbours (In the Priority Queue).
     Repeat the process while achieve destination vertex and all connected nodes are visited.
    */
    public Path getPath(Vertex source, Vertex destination) {
        Map<Vertex, Path> paths = new HashMap<>();
        paths.put(source, new Path());
        paths.get(source).setLevel(0);

        PriorityBlockingQueue<Vertex> queue = new PriorityBlockingQueue<>();
        queue.add(source);
        Path path;
        while (!queue.isEmpty()) {
            Vertex u = queue.poll();
            for (Edge neighbour : (List<Edge>) u.getNeighbours()) {
                if (paths.get(neighbour.getTarget()) == null) {
                    paths.put(neighbour.getTarget(), new Path());
                }
                int level = paths.get(u).getLevel() + 1;
                if (paths.get(neighbour.getTarget()).getLevel() > level) {
                    paths.get(neighbour.getTarget()).setLevel(level);
                    // Remove the node from the queue to update the distance value.
                    queue.remove(neighbour.getTarget());

                    paths.get(neighbour.getTarget()).getVertexes().add(u);
                    // Take the path visited till now and add the new node.s
                    paths.get(neighbour.getTarget()).setVertexes(new LinkedList<Vertex>(paths.get(u).getVertexes()));

                    //Reenter the node with new distance.
                    queue.add(neighbour.getTarget());
                    if (neighbour.getTarget().equals(destination)) {
                        break;
                    }
                }
            }
        }
        path = paths.get(destination);
        if (path == null) {
            path = new Path();
        }
        path.getVertexes().add(destination);
        return path;
    }

	public Vertex getVertex(int vertex){
		return vertices.get(vertex);
	}

    public List<Vertex> getVertexes() {
        return vertices;
    }

    public boolean isDirected() {
        return directed;
    }
}
