package graph.lib;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Vertex<Type> implements Comparable<Vertex> {
	private Type value;
	private String name;
    private List<Edge> neighbours;

    public Vertex() {
        neighbours =  new CopyOnWriteArrayList<>();
    }

    public Vertex(String name) {
        this();
        this.name = name;
        this.value = null;
    }

    public Vertex(String name, Type value) {
        this(name);
        this.value = value;
    }

    @Override
	public int compareTo(Vertex other) {
        if (!(this.getClassType().equals(other.getClassType()))) {
            return 1;
        } else if (name != null && !name.equals(other.name)) {
            return 1;
        } else if (value != null && !value.equals(other.value)) {
            return -1;
        }
        return 0;
    }

    @SuppressWarnings("unchecked")
    public String getClassType () {
        if(value != null) {
            return value.getClass().toGenericString();
        } else {
            return "Object";
        }
    }

    public List<Edge> getNeighbours() {
        return neighbours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getValue() {
        return value;
    }

    public void setValue(Type value) {
        this.value = value;
    }

    public String toString(){
		return (name == null ? "Vertex#Unnamed" : name) + (value == null ? "" : "(" + value + ")");
	}
}
