package graph.zrv;

/** Class represent the edges in the graph */
public class Edge {
	private final Vertex target;
	private final float weight;

	public Edge(Vertex target, float weight){
		this.target = target;
		this.weight = weight;
	}

    public Vertex getTarget() {
        return target;
    }

    public float getWeight() {
        return weight;
    }
}
