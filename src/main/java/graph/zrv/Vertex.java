package graph.zrv;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Vertex<Type> implements Comparable<Vertex> {
	private Type value;
	private String name;
    private List<Edge> neighbours;

    private  Vertex() {
        neighbours = new CopyOnWriteArrayList<>();
    }

    public Vertex(String name) {
        this();
        this.name = name;
        this.value = null;
    }

    public Vertex(String name, Type value) {
        this(name);
        this.value = value;
    }

    @Override
	public synchronized int compareTo(Vertex other){
	    if(!(this.getClassType().equals(other.getClassType()))) {
            return 1;
        } else if(name != null && !name.equals(other.name)) {
            return 1;
        } else if(value != null && !value.equals(other.value)) {
            return -1;
        } else {
            int countInclude = 0;
            for (Edge neighbour : neighbours) {
                if (!other.neighbours.contains(neighbour)) {
                    countInclude++;
                }
            }
            int countExclude = 0;
            for (Object neighbour : other.neighbours) {
                if (!neighbours.contains(neighbour)) {
                    countExclude--;
                }
            }
            if (countInclude != 0 || countExclude != 0) {
                return (countInclude == 0 ? 1 : countInclude) * (countExclude == 0 ? 1 : countExclude);
            } else {
                return countInclude * countExclude;
            }
        }
	}

    @SuppressWarnings("unchecked")
    private String getClassType () {
        if(value != null) {
            return value.getClass().toGenericString();
        } else {
            return "Object";
        }
    }

    public List<Edge> getNeighbours() {
        return neighbours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getValue() {
        return value;
    }

    public void setValue(Type value) {
        this.value = value;
    }

    public String toString(){
		return (name == null ? "Vertex#Unnamed" : name) + (value == null ? "" : "(" + value + ")");
	}
}
