package graph.zrv;

public interface Algorithm {
    Path getPath(Vertex source, Vertex destination);
}
