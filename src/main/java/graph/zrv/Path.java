package graph.zrv;

import java.util.LinkedList;
import java.util.List;

public class Path {
    private List<Vertex> vertexes = new LinkedList<>();
    private int level = Integer.MAX_VALUE - 1;
    private float minDistance = Float.POSITIVE_INFINITY;

    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public float getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(float minDistance) {
        this.minDistance = minDistance;
    }
}
