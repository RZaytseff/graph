package graph.zrv;
/**
 * Implementation of Dijkstra's Algorithm - Adjacency List and Priority Queue.
 */

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

/** find shortest path according summarise weight */
public class DijkstraAlgorithm implements Algorithm {
    /*
     Algorithm:
     1. Take the unvisited node with shortest path.
     2. Visit all its neighbours.
     3. Update the distances for all the neighbours (In the Priority Queue).
     Repeat the process while achieve destination vertex and all connected nodes are visited.
    */
    public Path getPath(Vertex source, Vertex destination) {
        Map<Vertex, Path> paths = new HashMap<>();
        paths.put(source, new Path());
        paths.get(source).setLevel(0);

        PriorityBlockingQueue<Vertex> queue = new PriorityBlockingQueue<>();
        queue.add(source);
        Path path = null;
        while (!queue.isEmpty()) {
            Vertex u = queue.poll();
//            synchronized (u.getNeighbours()) {
                for (Edge neighbour : (List<Edge>) u.getNeighbours()) {
                    int level = paths.get(u).getLevel() + 1;
                    if (paths.get(neighbour.getTarget()) == null) {
                        paths.put(neighbour.getTarget(), new Path());
                    }
                    if (paths.get(neighbour.getTarget()).getLevel() > level) {
                        // Remove the node from the queue to update the distance value.
                        queue.remove(neighbour.getTarget());
                        paths.get(neighbour.getTarget()).setLevel(level);

                        // Take the path visited till now and add the new node.s
                        paths.get(neighbour.getTarget()).setVertexes(new LinkedList<Vertex>(paths.get(u).getVertexes()));
                        paths.get(neighbour.getTarget()).getVertexes().add(u);

                        //Reenter the node with new distance.
                        queue.add(neighbour.getTarget());
                        if (neighbour.getTarget().equals(destination)) {
                            break;
                        }
                    }
                }
//            }
        }
        path = paths.get(destination);
        if (path == null) {
            path = new Path();
        }
        path.getVertexes().add(destination);
        return path;
    }
}

