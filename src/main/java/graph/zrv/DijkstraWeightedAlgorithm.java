package graph.zrv;
/**
 * Implementation of Dijkstra's Algorithm - Adjacency List and Priority Queue.
 */

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

/** find shortest path according summarise weight */
public class DijkstraWeightedAlgorithm implements Algorithm {

    public Path getPath(Vertex source, Vertex destination) {
        // Algorithm:
        // 1. Take the unvisited node with minimum weight.
        // 2. Visit all its neighbours.
        // 3. Update the distances for all the neighbours (In the Priority Queue).
        // Repeat the process while achieve destination vertex and all connected nodes are visited.

        Map<Vertex, Path> paths = new HashMap<>();
        paths.put(source, new Path());
        paths.get(source).setLevel(0);
        paths.get(source).setMinDistance(0);
        Path path = null;

        PriorityBlockingQueue<Vertex> queue = new PriorityBlockingQueue<>();
        queue.add(source);

        while(!queue.isEmpty()) {
            Vertex u = queue.poll();
                for (Edge neighbour : (List<Edge>) u.getNeighbours()) {
                    int newLevel = paths.get(u).getLevel();
                    float newDist = paths.get(u).getMinDistance() + neighbour.getWeight();
                    if (paths.get(neighbour.getTarget()) == null) {
                        paths.put(neighbour.getTarget(), new Path());
                    }
                    if (paths.get(neighbour.getTarget()).getMinDistance() > newDist) {
                        // Remove the node from the queue to update the distance value.
                        queue.remove(neighbour.getTarget());
                        paths.get(neighbour.getTarget()).setLevel(++newLevel);
                        paths.get(neighbour.getTarget()).setMinDistance(newDist);

                        // Take the path visited till now and add the new vertex
                        paths.get(neighbour.getTarget()).setVertexes(new LinkedList<Vertex>(paths.get(u).getVertexes()));
                        paths.get(neighbour.getTarget()).getVertexes().add(u);

                        //Reenter the node with new distance.
                        queue.add(neighbour.getTarget());
                        if (neighbour.getTarget().equals(destination)) {
                            break;
                        }
                    }
                }
        }
        path = paths.get(destination);
        if (path == null) {
            path = new Path();
            path.getVertexes().add(source);
        }
        path.getVertexes().add(destination);
        return path;
    }
}

