package graph.zrv;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class Graph {
    private boolean directed;
    private boolean weighted;
    private Algorithm algorithm;

    /** Each vertex maps to a list of all his neighbors */
	private List<Vertex> vertices;

    public Graph(){
        this.vertices = new CopyOnWriteArrayList<>();
    }

    public Graph(boolean directed){
        this();
        this.directed = directed;
	}


    public Graph(boolean directed, boolean weighted) {
        this(directed);
        this.weighted = weighted;
        if(weighted) {
            algorithm = new DijkstraWeightedAlgorithm();
        } else {
            algorithm = new DijkstraAlgorithm();
        }
    }

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
    }

    public boolean deleteVertex(Vertex vertex) {
            if(vertices.contains(vertex)) {
                vertices.remove(vertex);
                return true;
            } else {
                return false;
            }
    }

    public boolean addEdge(int src, int dest, int weight) {
        if (src >= vertices.size()) {
            return false;
        }
        Vertex s = vertices.get(src);
        if (dest >= vertices.size()) {
            return false;
        }
        Vertex d = vertices.get(dest);
        if (s == null || d == null) {
            return false;
        }
        Edge edge = new Edge(d, weight);
        s.getNeighbours().add(edge);

        if (!directed) {
            Edge return_edge = new Edge(s, weight);
            d.getNeighbours().add(return_edge);
        }
        return true;
	}

	public void traverse(Consumer<Vertex> f) {
        for (Vertex vertex : vertices) {
            System.out.print("For " + vertex);
            f.accept(vertex);
            System.out.println(" result is " + vertex);
        }
    }

    public Path getPath(Vertex source, Vertex destination) {
        return algorithm.getPath(source, destination);
    }

    List<Vertex> getVertices() {
		return vertices;
	}

	public Vertex getVertex(int vertex) {
        if (vertex >= vertices.size()) {
            return null;
        }
        return vertices.get(vertex);
	}

}
