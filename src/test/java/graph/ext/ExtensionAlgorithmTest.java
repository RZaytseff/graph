package graph.ext;

import graph.zrv.AlgorithmTest;
import org.junit.Test;

import java.util.Date;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class ExtensionAlgorithmTest {
    private Graph graph;
    private volatile PathResult pathResult;
    private class PathResult {
        int level;
        double distance;
    }

    private int[][][] weightedEdgeMatrix = { {{0,1,4}, {0,7,8}}
                                        , {{1,2,8}, {1,7,11}}
                                        , {{2,3,7}, {2,8,4}, {2,8,2}}
                                        , {{3,5,14},{3,4,9}}
                                        , {{4,5,10}}
                                        , {{5,6,2}}
                                        , {{6,7,1}, {6,8,6}}
                                        , {{7,8,7}}
                                        , {{8,0,15}, {8,4,15}}
                                        };

    private boolean repeat;
    private boolean assertThread;

    /** show or hide debug logging messages */
    private boolean debugLog = true;

    /** Create and fill a new graph. */
    private void createGraph(boolean directed, boolean weighted) {
        graph = (Graph)new GraphFabric(directed, weighted);
        fillGraph();
    }

    private void fillGraph() {
        int numberVertices = 9;
        graph.addVertex(new Vertex<Integer>("Vertex#0-Integer"));
        for (int i = 1; i < numberVertices; i++) {
            graph.addVertex(new Vertex<Float>("Vertex#" + Integer.toString(i) + "-Float  ", new Float(i)));
        }
        // Add the required edges.
        for (int[][] edges : weightedEdgeMatrix) {
            addEdges(edges);
        }
    }

    private void addEdges(int[][] edges) {
        for (int[] weightedEdge : edges) {
            graph.addEdge(weightedEdge[0], weightedEdge[1], weightedEdge[2]);
        }

    }

    /** look up a path between source with sourceIndex and destination wth destinationIndex */
    private PathResult pathResult(int sourceIndex, int destinationIndex) {
        Vertex source = graph.getVertex(sourceIndex);
        Vertex destination = graph.getVertex(destinationIndex);
        Path path = graph.getPath(source, destination);
        if(debugLog) {
            // Print steps count and minimum Distance.
            System.out.print("" + source + " -> " + destination + " , Path steps = " + path.getLevel() + " , Path Distance = " + path.getMinDistance() + ", Path: ");
        }
        for(Vertex v: path.getVertexes()){
            if(v.equals(source)) {
                System.out.print("" + v);
            } else {
                System.out.print(" -> " + v);
            }
        }
        System.out.println(" ");

        PathResult pathResult = new PathResult();
        pathResult.level = path.getLevel();
        pathResult.distance = path.getMinDistance();
        return pathResult;
    }

    @Test
    /** Calculate shortest path between two edges in UnDirect Graph. */
    public void unDirectGraphTest() {
        System.out.println("\n-----------------> Begin of UnDirect Graph Test ");
        Long timeBegin = new Date().getTime();
        // Create a new graph.
        createGraph(false, false);
        int directLevel = pathResult(4, 0).level;
        assertEquals(2, directLevel);

        int reverceLevel =  pathResult(0, 4).level;
        assertEquals(2, reverceLevel);

        assertEquals(directLevel, reverceLevel);
        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of UnDirect Graph Test: Duration = " + (timeEnd - timeBegin));
    }

    @Test
    /** Calculate shortest path between two edges in Directed Graph. */
    public void directedGraphTest() {
        System.out.println("\n-----------------> Begin of Directed Graph Test ");
        Long timeBegin = new Date().getTime();
        // Create a new graph.
        createGraph(true, false);

        int directLevel = pathResult(4, 0).level;
        assertEquals(4, directLevel);

        int reverceLevel =  pathResult(0, 4).level;
        assertEquals(3, reverceLevel);

        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of Directed Graph Test: Duration = " + (timeEnd - timeBegin));
    }

    @Test
    /** Calculate minimal weightiest path between two edges in UnDirected and Weighted Graph. */
    public void unDirectedWeightedGraphTest() {
        System.out.println("\n-----------------> Begin of UnDirected and Weighted Graph Test ");
        Long timeBegin = new Date().getTime();
        // Create and fill a new graph.
        createGraph(false, true);

        pathResult = pathResult(4, 0);
        int directLevel = pathResult.level;
        assertEquals(4, directLevel);
        double directDistance = pathResult.distance;
        assertEquals(21, directDistance, new Double(0.0000001));

        pathResult = pathResult(0, 4);
        int reverceLevel = pathResult.level;
        assertEquals(4, reverceLevel);
        double reverceDistance = pathResult.distance;
        assertEquals(21, reverceDistance, new Double(0.0000001));

        assertEquals(directLevel, reverceLevel);
        assertEquals(directDistance, reverceDistance, new Double(0.0000001));

        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of UnDirected and Weighted Graph Test: Duration = " + (timeEnd - timeBegin));
    }

    @Test
    /** Calculate minimal weightiest path between two edges in Directed and Weighted Graph. */
    public void directedWeightedGraphTest() {
        System.out.println("\n-----------------> Begin of Directed and Weighted Graph Test ");
        Long timeBegin = new Date().getTime();
        // Create and fill a new Directed and Weighted graph.
        createGraph(true, true);

        pathResult = pathResult(4, 0);
        int directLevel = pathResult.level;
        assertEquals(4, directLevel, new Double(0.0000001));
        double directDistance = pathResult.distance;
        assertEquals(33, directDistance, new Double(0.0000001));

        pathResult = pathResult(0, 4);
        int reverceLevel = pathResult.level;
        assertEquals(4, reverceLevel);
        double reverceDistance = pathResult.distance;
        assertEquals(28, reverceDistance, new Double(0.0000001));

        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of Directed and Weighted Graph Test: Duration = " + (timeEnd - timeBegin));
    }

    @Test
    /** traverse graph and consume action result for any vertex */
    public void traverseTest() {
        System.out.println("\n-----------------> Begin of traverse of Graph Test ");
        Long timeBegin = new Date().getTime();
        // Create a new graph.
        createGraph(true, true);
        Consumer<Vertex> someUserConsumer = (vertex) -> vertex.setName(vertex.getName() + ".result");
        graph.traverse(someUserConsumer);
        for(Vertex vertex: graph.getVertexes()) {
            assertTrue(vertex.getName().endsWith(".result"));
        }

        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of traverse of Graph  Test: Duration = " + (timeEnd - timeBegin));
    }

//----------------  concurrent tests group --------------------------------------*/

    @Test
    /** Calculate shortest path between two edges in UnDirected Graph. */
    public void unDirectedGraphConcurrentTest() {
        System.out.println("\n-----------------> Begin of UnDirected Graph Concurrent Test ");
        System.out.println("-----------------> End of UnDirected Graph Concurrent Test: Duration = " + createGraphAndStartConcurrentTest(false, false));
    }

    @Test
    /** Calculate shortest path between two edges in Directed Graph. */
    public void directedGraphConcurrentTest() {
        System.out.println("\n-----------------> Begin of Directed Graph Concurrent Test ");
        System.out.println("-----------------> End of Directed Graph Concurrent Test: Duration = " + createGraphAndStartConcurrentTest(true, false));
    }

    @Test
    /** Calculate shortest path between two edges in UnDirected and Weighted Graph. */
    public void unDirectedWeightedGraphConcurrentTest() {
        System.out.println("\n-----------------> Begin of UnDirected and Weighted Graph Concurrent Test ");
        System.out.println("-----------------> End of UnDirected and Weighted Graph Concurrent Test: Duration = " + createGraphAndStartConcurrentTest(false, true));
    }

    @Test
    /** Calculate shortest path between two edges in Directed and Weighted Graph. */
    public void directWeightedGraphConcurrentTest() {
        System.out.println("\n-----------------> Begin of Directed and Weighted Graph Concurrent Test ");
        System.out.println("-----------------> End of Directed and Weighted Graph Concurrent Test: Duration = " + createGraphAndStartConcurrentTest(true, true));
    }

    private Long createGraphAndStartConcurrentTest(boolean directed, boolean weighted) {
        Long timeBegin = new Date().getTime();
        assertThread = true;
        // Create a new graph.
        createGraph(directed, weighted);
        startConcurrentTest();
        if(!assertThread) {
            fail();
        } else {
            assertTrue(true);
        }
        Long timeEnd = new Date().getTime();
        return timeEnd - timeBegin;
    }

    private void startConcurrentTest() {
        repeat = true;
        Thread directPath  = new Thread(new getPathConcurrent(0, 4));
        Thread revercePath = new Thread(new getPathConcurrent(4, 0));
        Thread transformFirst   = new Thread(new concurrentTransform());
        Thread transformSecond  = new Thread(new concurrentTransform());
        Thread traverce    = new Thread(new concurrentTraverce());
        try {
            traverce.start();
            directPath.start();
            transformFirst.start();
            revercePath.start();
            transformSecond.start();
            Thread.sleep(1000);
        } catch(Exception e) {
            e.printStackTrace();
            assertThread = false;
        }
        repeat = false;
    }

    private class getPathConcurrent implements Runnable {
        int sourceIndex;
        int destinationIndex;
        getPathConcurrent(int sourceIndex, int destinationIndex) {
            this.sourceIndex = sourceIndex;
            this.destinationIndex = destinationIndex;
        }

        public void run() {
            while(repeat) {
                try {
                    Thread.sleep(15);
                    pathResult = pathResult(sourceIndex, destinationIndex);
                } catch (Exception e) {
                    e.printStackTrace();
                    assertThread = false;
                }
            }
        }
    }

    private class concurrentTransform implements Runnable {
        public void run() {
            while(repeat) {
                for(int i = 0; i < 9; i++) {
                    Vertex vertex = graph.getVertex(i);
                    try {
                        Thread.sleep(10);
                        graph.deleteVertex(vertex);
                        graph.addVertex(new Vertex<>("Vertex#" + Integer.toString(i) + "-Float  ", new Float(i)));
                        addEdges(weightedEdgeMatrix[i]);
                    } catch (Exception e) {
                        e.printStackTrace();
                        assertThread = false;
                    }

                }
            }
        }
    }

    private class concurrentTraverce implements Runnable {
        private Consumer<Vertex> someUserConsumer = (vertex) -> {vertex.setName(vertex.getName() + ".result"); vertex.setValue((vertex.getValue() != null ? (Float)vertex.getValue() + 10 : 10));};
        public void run() {
            while(repeat) {
                try {
                graph.traverse(someUserConsumer);
                synchronized (graph.getVertexes()) {
                    for (Vertex vertex : graph.getVertexes()) {
                        assertTrue((vertex.getName().endsWith(".result") || vertex.getName().startsWith("Vertex")));
                    }
                }
                Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                    assertThread = false;
                }
            }
        }
    }

}

