package graph.lib;

import java.util.Date;

import org.junit.Test;
import static org.junit.Assert.*;

public class LibraryAlgorithmTest {
    private Graph graph;
    int level;

    private int[][][] weightedEdgeMatrix = { {{0,1}, {0,7}}
                                        , {{1,2}, {1,7}}
                                        , {{2,3}, {2,8}, {2,8}}
                                        , {{3,5},{3,4}}
                                        , {{4,5}}
                                        , {{5,6}}
                                        , {{6,7}, {6,8}}
                                        , {{7,8}}
                                        , {{8,0}, {8,4}}
                                        };

    private boolean repeat;
    private boolean assertThread;

    /** show or hide debug logging messages */
    private boolean debugLog = true;

    /** Create and fill a new graph. */
    private void createGraph(boolean directed) {
        graph = new GraphFabric(directed);
        fillGraph();
    }

    private void fillGraph() {
        int numberVertices = 9;
        graph.addVertex(new Vertex<Integer>("Vertex#0-Integer"));
        graph.addVertex(new Vertex<Double>("Vertex#1-Double", 1D));
        for (int i = 2; i < numberVertices; i++) {
            graph.addVertex(new Vertex<Float>("Vertex#" + Integer.toString(i) + "-Float", new Float(i)));
        }
        // Add the required edges.
        for (int[][] edges : weightedEdgeMatrix) {
            addEdges(edges);
        }
    }

    private void addEdges(int[][] edges) {
        for (int[] weightedEdge : edges) {
            graph.addEdge(weightedEdge[0], weightedEdge[1]);
        }

    }

    /** look up a path between source with sourceIndex and destination wth destinationIndex */
    private int pathLevel(int sourceIndex, int destinationIndex) {
        Vertex source = graph.getVertex(sourceIndex);
        Vertex destination = graph.getVertex(destinationIndex);
        Path path = graph.getPath(source, destination);
        if(debugLog) {
            // Print steps count and minimum Distance.
            System.out.print("" + source + " -> " + destination + ", Path steps = " + path.getLevel() + ", Path: ");
        }
        for(Vertex v: path.getVertexes()){
            if(v.equals(source)) {
                System.out.print("" + v);
            } else {
                System.out.print(" -> " + v);
            }
        }
        System.out.println(" ");
        return path.getLevel();
    }

    @Test
    /** Calculate shortest path between two edges in UnDirect Graph. */
    public void unDirectGraphTest() {
        System.out.println("\n-----------------> Begin of UnDirect Graph Test");
        Long timeBegin = new Date().getTime();
        // Create a new graph.
        createGraph(false);
        int directLevel = pathLevel(4, 0);
        assertEquals(2, directLevel);

        int reverceLevel =  pathLevel(0, 4);
        assertEquals(2, reverceLevel);

        assertEquals(directLevel, reverceLevel);
        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of UnDirect Graph Test: Duration = " + (timeEnd - timeBegin));
    }

    @Test
    /** Calculate shortest path between two edges in Directed Graph. */
    public void directedGraphTest() {
        System.out.println("\n-----------------> Begin of Directed Graph Test");
        Long timeBegin = new Date().getTime();
        // Create a new graph.
        createGraph(true);

        int directLevel = pathLevel(4, 0);
        assertEquals(4, directLevel);

        int reverceLevel =  pathLevel(0, 4);
        assertEquals(3, reverceLevel);

        Long timeEnd = new Date().getTime();
        System.out.println("-----------------> End of Directed Graph Test: Duration = " + (timeEnd - timeBegin));
    }

//----------------  concurrent tests group --------------------------------------*/

    @Test
    /** Calculate shortest path between two edges in UnDirected Graph. */
    public void unDirectedGraphConcurrentTest() {
        System.out.println("\n-----------------> Begin of UnDirected Graph Concurrent Test");
        System.out.println("-----------------> End of UnDirected Graph Concurrent Test: Duration = " + createGraphAndStartConcurrentTest(false, false));
    }

    @Test
    /** Calculate shortest path between two edges in Directed Graph. */
    public void directedGraphConcurrentTest() {
        System.out.println("\n-----------------> Begin of Directed Graph Concurrent Test");
        System.out.println("-----------------> End of Directed Graph Concurrent Test: Duration = " + createGraphAndStartConcurrentTest(true, false));
    }

    private Long createGraphAndStartConcurrentTest(boolean directed, boolean weighted) {
        Long timeBegin = new Date().getTime();
        assertThread = true;
        // Create a new graph.
        createGraph(directed);
        startConcurrentTest();
        if(!assertThread) {
            fail();
        } else {
            assertTrue(true);
        }
        Long timeEnd = new Date().getTime();
        return timeEnd - timeBegin;
    }

    private void startConcurrentTest() {
        repeat = true;
        Thread transform   = new Thread(new concurrentTransform());
        transform.start();
        Thread directPath  = new Thread(new getPathConcurrent(0, 4));
        directPath.start();
        Thread revercePath = new Thread(new getPathConcurrent(4, 0));
        revercePath.start();
        try {
            Thread.sleep(1000);
        } catch(Exception e) {
            e.printStackTrace();
            assertThread = false;
        }
        repeat = false;
    }

    private class getPathConcurrent implements Runnable {
        int sourceIndex;
        int destinationIndex;
        getPathConcurrent(int sourceIndex, int destinationIndex) {
            this.sourceIndex = sourceIndex;
            this.destinationIndex = destinationIndex;
        }

        public void run() {
            while(repeat) {
                try {
                    Thread.sleep(10);
                    level = pathLevel(sourceIndex, destinationIndex);
                } catch (Exception e) {
                    e.printStackTrace();
                    assertThread = false;
                }
            }
        }
    }

    private class concurrentTransform implements Runnable {
        public void run() {
            while(repeat) {
                for(int i = 0; i < 9; i++) {
                    Vertex vertex = graph.getVertex(i);
                    try {
                        Thread.sleep(15);
                        graph.deleteVertex(vertex);
                        graph.addVertex(new Vertex<>("Vertex#" + Integer.toString(i) + "-Float", new Float(i)));
                        addEdges(weightedEdgeMatrix[i]);
                    } catch (Exception e) {
                        e.printStackTrace();
                        assertThread = false;
                    }

                }
            }
        }
    }
}

